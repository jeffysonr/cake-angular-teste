<?php
App::uses('AppController', 'Controller');
/**
 * Produtos Controller
 *
 * @property Produto $Produto
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProdutosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Produto->recursive = 0;
		$this->set('produtos', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Produto->exists($id)) {
			throw new NotFoundException(__('Invalid produto'));
		}
		$options = array('conditions' => array('Produto.' . $this->Produto->primaryKey => $id));
		$this->set('produto', $this->Produto->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Produto->create();
			if ($this->Produto->save($this->request->data)) {
				$this->Session->setFlash(__('The produto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The produto could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Produto->exists($id)) {
			throw new NotFoundException(__('Invalid produto'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Produto->save($this->request->data)) {
				$this->Session->setFlash(__('The produto has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The produto could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Produto.' . $this->Produto->primaryKey => $id));
			$this->request->data = $this->Produto->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Produto->id = $id;
		if (!$this->Produto->exists()) {
			throw new NotFoundException(__('Invalid produto'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Produto->delete()) {
			$this->Session->setFlash(__('The produto has been deleted.'));
		} else {
			$this->Session->setFlash(__('The produto could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


	public function json_listar() {
		$this->layout = 'ajax';
		
		$this->set('produtos', $this->Produto->find('all'));
	}

	public function json_save($typeOf = null) {
		$this->layout = 'ajax';
		$result = array();
		$result['ok'] = false;
		
		if ($this->request->is(array('put', 'post'))) {
			if ($this->Produto->save($this->request->data)) {
				$result['ok'] = true;
				$result['message'] = __('The produto has been saved.');

				if ($typeOf === 'add') {
					$result['obj'] = $this->Produto->read(null, $this->Produto->id);
				}
			} else {
				$result['message'] = __('The produto could not be saved. Please, try again.');
			}
		}

		echo json_encode($result);
		die;
	}

	public function json_delete() {
		$this->layout = 'ajax';
		$result = array();
		$result['ok'] = false;

		$this->Produto->id = $this->request->data;
		
		if ($this->Produto->exists()) {
			$this->request->allowMethod('post', 'delete');

			if ($this->Produto->delete()) {
				$result['ok'] = true;
				$result['message'] = __('The produto has been deleted.');
			} else {
				$result['message'] = __('The produto could not be deleted. Please, try again.');
			}
		} else {
			$result['message'] = __('Invalid produto');	
		}
		
		echo json_encode($result);
		die;
	}
}
