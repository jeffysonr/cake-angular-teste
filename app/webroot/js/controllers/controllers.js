myApp.controller('produtosController', function($scope, $http, ngTableParams){

	$http.get('/json/produtos/listar').success(function(data){
		produtos = data;
		$scope.produtos = produtos;

		paginacao($scope, ngTableParams, produtos, 10);
	});

	$scope.edit = function(index) {
		$scope.tr_edit = true;
		$scope.produto_edit = index;
	}

	$scope.cancel = function() {
		$scope.tr_edit = false;
	}

	$scope.add = function() {
		$http({
            method : 'POST',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url : '/json/produtos/save/add',
            data : $.param({'data' : $scope.produto_add}),
            
        }).success(function(data){
        	if (data.ok) {
        		produtos.unshift(data.obj);
        		$scope.produto_add = '';
        	}

			showMessage(data.ok, data.message);
			$scope.cadastrar = false;
		});
	}

	$scope.update = function(index) {
		$http({
            method : 'PUT',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url : '/json/produtos/save/edit',
            data : $.param({'data' : index}),
            
        }).success(function(data){
			showMessage(data.ok, data.message);
			$scope.tr_edit = false;
		});
	}

	$scope.delete = function(index) {

		if (confirm('Deseja realmente deletar o registro #'+index.id)) {
			$http({
	            method : 'DELETE',
	            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	            url : '/json/produtos/delete',
	            data : $.param({'data' : index.id}),
	            
	        }).success(function(data){
	        	if (data.ok) {
	        		deleteArray (produtos, index);
				}
	        	
	        	showMessage(data.ok, data.message);
	        });
    	}
	}

});
