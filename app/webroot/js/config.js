var $=jQuery;

var myApp = angular.module('myApp', ['ngTable']);

var produtos=[];

function paginacao ($scope, ngTableParams, arrayPaginacao, porPagina) {
	$scope.tableParams = new ngTableParams({
		page: 1,
		count: porPagina
	},{
		total: arrayPaginacao.length,
		getData: function($defer, params) {
            $defer.resolve(arrayPaginacao.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
	});
}

function showMessage (ok, message) {
	if (ok) {
		$('#message').addClass('alert alert-success');
	} else {
		$('#message').addClass('alert alert-danger');
	}
	
	$('#message').show('fast');
	$('#message').html(message);

	setTimeout(function(){
		$('#message').hide('fast');
		$('#message').removeClass('alert alert-success');
		$('#message').removeClass('alert alert-danger');
	}, 2000);
}

function deleteArray (array, index) {
	var indexof = array.indexOf(index);
	
	array.splice(indexof,1);
}