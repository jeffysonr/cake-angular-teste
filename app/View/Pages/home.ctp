<?php
  echo $this->Html->script(array('ngtable', 'config', 'controllers/controllers'), array('inline'=>false));
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" ng-controller="produtosController">
  <h1 class="page-header">Listar Produtos</h1>

  <div id="formCadastrar" class="row">
    
    <div class="col-md-9">
      <form class="form-horizontal" ng-show="cadastrar">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Nome</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="Nome" ng-model="produto_add.Produto.nome" />
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label">Preço</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="Preço" ng-model="produto_add.Produto.preco">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success" ng-click="add()">Cadastrar</button>
          </div>
        </div>
      </form>
    </div>

    <div class="col-sm-offset-1 col-md-2">
      <button type="button" class="btn btn-default btn-sm btn-block" ng-click="cadastrar = !cadastrar">Cadastrar Produto</button>
    </div>
  </div>
  
  <div id="message" role="alert" style="display:none; text-align:center;"></div>

  <div class="table-responsive row">
    <table class="table table-striped" ng-table="tableParams">
      <thead>
        <tr>
          <th>Nome</th>
          <th>Preço</th>
          <th>Data</th>
          <th width="8%">#</th>
          <th width="8%">#</th>
        </tr>
      </thead>
      <tbody>
        
        <tr ng-show="tr_edit">
          <td><input type="text" ng-model="produto_edit.Produto.nome" /></td>
          <td><input type="text" ng-model="produto_edit.Produto.preco" /></td>
          <td>--</td>
          <td><button type="button" class="btn btn-success" ng-click="update(produto_edit)">Atualizar</button></td>
          <td><button type="button" class="btn btn-warning" ng-click="cancel()">Cancelar</button></td>
        </tr>

        <tr ng-repeat="produto in $data | filter: busca">
          <td>{{produto.Produto.nome | uppercase}}</td>
          <td>{{produto.Produto.preco | currency: "R$ "}}</td>
          <td>--</td>
          <td><button type="button" class="btn btn-primary" ng-click="edit(produto)">Editar</button></td>
          <td><button type="button" class="btn btn-danger" ng-click="delete(produto.Produto)">Deletar</button></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>