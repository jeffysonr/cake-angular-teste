
<!DOCTYPE html>
<html lang="en" ng-app="myApp">
  <head>
    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Curso Angular - Cakephp, Dashboard Template for Bootstrap">
    <meta name="author" content="Jeffyson">
    <link rel="icon" href="../../favicon.ico">

    <title>Curso Angular - Cakephp</title>

    <?php
      echo $this->Html->meta('icon');

      echo $this->Html->css(array('bootstrap', 'dashboard'));

      echo $this->fetch('meta');
      echo $this->fetch('css');
    ?>
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">AngularJS App</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Buscar..." ng-model="busca">
          </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
            <li><a href="/produtos">Produtos</a></li>
            <li><a href="/json/produtos/listar">Produtos (Json)</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="">Nav item</a></li>
            <li><a href="">Nav item again</a></li>
            <li><a href="">One more nav</a></li>
            <li><a href="">Another nav item</a></li>
            <li><a href="">More navigation</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="">Nav item again</a></li>
            <li><a href="">One more nav</a></li>
            <li><a href="">Another nav item</a></li>
          </ul>
        </div>
        
        <?php echo $this->fetch('content'); ?>

      </div>
    </div>

    <?php echo $this->element('sql_dump'); ?>

    <?php echo $this->Html->script(array('angular', 'ui-utils', 'ui-bootstrap')); ?>
    <?php echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'); ?>
    <?php echo $this->fetch('script'); ?>
  </body>
</html>
